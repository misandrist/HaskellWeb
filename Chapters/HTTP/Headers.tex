\section{Headers\label{sec:http:headers}}

Both \hyperref[sec:http:rq]{Requests} and
\hyperref[sec:http:rs]{Responses} include Headers. These headers
contain information such as the name of the host sent by the client,
the type of information contained in the request body, the length of
the information, the URL containing the link the broser followed, and
many others. These are especially important in the response since the
browser will use these to understand what kind of information the
server is sending and how to display it.

This first example demonstrates using both kinds of headers.

\lstinputlisting[caption=Show the client User-Agent header.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

First we look up the \CT\ header:

\lstinputlisting[linerange=17-18,firstnumber=17,caption=Find the Request's \CT\ header.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

Next we set the body response \CT\ header:

\lstinputlisting[linerange=16-16,firstnumber=15,caption=Set the Response \CT.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

Finally we send whatever the request \CT\ was, or if there wasn't
anything there we mention that instead:

\lstinputlisting[linerange=14-15,firstnumber=14,caption=Send the Response with the Request's \CT.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

\subsection{\Maybe\label{sec:http:headers:maybe}}

This example demonstrates the \maybe\ function and corresponding
\Maybe\ value. 

\lstinputlisting[linerange=15-15,firstnumber=15,caption=The \maybe\ function.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

\Maybe\ is a way of expressing conditional branches as data rather
than just code. The compiler can then use the data itself to check
that the code accounts for each possibility. An entire class of
runtime error that includes missing methods, null object,
uninitialized pointers, and others isn't even possible to write in
code anymore. 

A \Maybe\ value can represent one of two things: \Nothing, the case
where there's no underlying value, and \Just{value}, where there is an
underlying value. It's similar to \Literal{if \dots\ then \dots\ else}
in other languages, but since \Maybe\ ``contains'' the value the code
itself has to only use the value when it exists. The \maybe\ function
is one function that can do this for us.

We're interested in the value of the \CT\ header sent by the
browser. Since there's nothing enforcing that there will be a \CT\
header, the request may not have it, and we need to be prepared. The
\DLlookup\ function, is the first step. It will search a list of
key/value pairs for a key. If the key is found, it will return
\Just{value}, otherwise it'll return \Nothing.

\lstinputlisting[linerange=17-17,firstnumber=17,caption=The \DLlookup\ function.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

The second step is actually returning a value that we can use in
building a response. This is where the \maybe\ function comes it. We
call \maybe\ with a value to use if we have \Nothing, a function to
call on the value if we have \Just{value}, and a \Maybe, in this case
the \Maybe\ from \DLlookup.

\lstinputlisting[linerange=15-15,firstnumber=15,caption=The \maybe\ function explained.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

In our case, we're using the \ID\ function. All this does is return
whatever it's passed. So this \maybe\ call will be either the \CT\
header value itself, or if there's no \CT\ header, it'll be the string
``No Content-Type in request.''.

Another interesting thing about \Maybe\ is that it can run a function
on the \Literal{value} if it's a \Just{value}, or skip the function if
it's \Nothing. The \CT\ header from the browser is a \StrictByteString
while the body expects a \LazyByteStringT. To convert them we can use
the \fromStrict\ function, but this can't take a \Maybe. It can only
take a \StrictByteString. With \Maybe, we can use \fmapOp\ to run
\fromStrict\ on the \Literal{value}\ itself if there is one, or skip
calling it if it's \Nothing. This is another way \Maybe\ prevents even
writing the entire ``Missing Value'' class of runtime errors.

\lstinputlisting[linerange=17-17,firstnumber=17,caption=Example of \Maybe\ and \fmapOp.]{HaskellWebCode/Source/HTTP/RequestHeader.hs}

\subsection{Exercises}

\begin{Exercise}[difficulty=1,title=Show Content-Length Header]
Instead of the User-Agent header, show the Content-Length Header.
\end{Exercise}

\begin{Exercise}[difficulty=3,title=Show all Headers]
Show all of the headers in the request to the browser instead of just a single header.
\end{Exercise}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../HaskellWeb"
%%% End:
