\section{Responses\label{sec:http:rs}}

The \Literal{Response} is what the browser uses to understand what
it's getting back from the web application. 

\subsection{Status Codes\label{sec:http:rs:codes}}

The first thing the browser will get is the
\href{https://tools.ietf.org/html/rfc7231#section-6}{Response
  Code}. Here are some \hyperref[tab:http:rs:codes]{common status codes}.

\begin{table}[h]
\begin{tabular}{|l|l|p{2.5in}|}
  \hline
  Code & Status & Notes \\
  \hline
  \href{https://tools.ietf.org/html/rfc7231#section-6.3.1}{200} & OK & The request was successful. The response body nearly always contains the requested content. \\
  \href{https://tools.ietf.org/html/rfc7235#section-3.1}{401} & Unauthorized & The person will need to authenticate with the server to access this content. \\
  \href{https://tools.ietf.org/html/rfc7231#section-6.5.3}{403} & Forbidden & Even with authentication the server can't provide the person access to this content. \\
  \href{https://tools.ietf.org/html/rfc7231#section-6.5.4}{404} & Not Found & No document was found for the requested link. \\
  \href{https://tools.ietf.org/html/rfc7231#section-6.6.1}{500} & Internal Server Error & There's an unhandled programming error while satisfying the request. \\
\hline
\end{tabular}\caption{Common HTTP Status Codes\label{tab:http:rs:codes}}
\end{table}

This example shows how to set the response status and also writing a
message back to the client that matches the status code. It also
provides a default handler for requests with no route that returns a
404 status.

\lstinputlisting[caption=Setting the Response status code (HTTPStatusCodes).]{HaskellWebCode/Source/HTTP/StatusCodes.hs}

\subsubsection{Exercises}

\begin{Exercise}[difficulty=1,title=Add another status code.]
Add another route for another status code from \href{https://tools.ietf.org/html/rfc7231#section-6}{RFC 7231}.
\end{Exercise}

\begin{Exercise}[difficulty=1,title=Change response body.]
    Combine the numeric code and the message as a string in the
    body. For example, instead of ``OK'', response 200 will return
    ``200 OK''.
\end{Exercise}

\subsection{Content Type\label{sec:http:rs:ct}}

The \CT tells the browser what kind of document it's receiving from
the web application. There is a
\href{https://www.iana.org/assignments/media-types/media-types.xhtml}{comprehensive
  list} maintained by the \IANA. The \CT is a response
\hyperref[sec:http:headers]{Header} set by the web application. Some common \CT{}s are:

\begin{itemize}
\item \href{http://tools.ietf.org/html/rfc2046#section-4.1.3}{\Literal{text/plain}}
\item \href{https://www.iana.org/assignments/media-types/text/html}{\Literal{text/html}}
\item \href{https://www.iana.org/assignments/media-types/image/png}{\Literal{image/png}}
\end{itemize}

This next example shows how the \CT can be set explicitly in a
handler. It also shows the
\href{http://hackage.haskell.org/package/snap-core-0.9.7.0/docs/Snap-Core.html#v:sendFile}{\Literal{sendFile}}
being used to send a disk file to the browser. The
\href{http://hackage.haskell.org/package/snap-core-0.9.7.0/docs/Snap-Core.html#v:ifTop}{\Literal{ifTop}}
function pins the \Literal{``/''} route to just the root path of the
site. Normally routes will match as much as possible of a path, but
this forces this route to match only the empty root path.

\lstinputlisting[caption=Setting the \Literal{Content-Type} (HTTPContentType).]{HaskellWebCode/Source/HTTP/ContentType.hs}

\subsubsection{Exercises}

\begin{Exercise}[difficulty=1,title=Change the image name.]
Edit the HTML and the routes with a different name for the image.
\end{Exercise}

\begin{Exercise}[difficulty=2,title=Styling.]
    Add a style sheet for the html to change the \Literal{<h1>} and
    the image styles.
\end{Exercise}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../HaskellWeb"
%%% End:
